
import Vue from 'vue'
import App from './App.vue'

const axios = require('axios').default;
import router from './router.js'
import Cookies from 'js-cookie'

Vue.config.productionTip = false
axios.defaults.xsrfCookieName = 'csrftoken'
axios.defaults.xsrfHeaderName = "X-CSRFToken"
axios.defaults.withCredentials = true
axios.defaults.baseURL = 'https://127.0.0.1:8000';
/*axios.interceptors.request.use(function (config) {
  //console.log(document.cookie.split(';').filter((item) => item.includes('csrftoken'))[0])
  console.log(Cookies.get('csrftoken'))
  config.headers.common['X-CSRFToken'] = document.cookie.split(';').filter((item) => item.includes('csrftoken'))[0].split('=')[1]
  return config;
}, function (error) {
  // Do something with request error
  return Promise.reject(error);
});*/
/*axios.get("http://localhost:8000")
              .then(response => {
                var template = document.createElement('template');//you could elements other than <template>
                template.innerHTML = response.data;
                const token = template.content.querySelector('[name=csrfmiddlewaretoken]').value;
                axios.defaults.headers.common['X-CSRFToken'] = token
              })
              .catch(error => {
                console.log(error + "")
            })
            */
new Vue({
  render: h => h(App),
  router
}).$mount('#app')