import Vue from 'vue'
import Router from 'vue-router'
import Meta from 'vue-meta'

import LandingPage from './views/landing-page'
import Login from './views/login'
import './style.css'
import VueCookies from 'vue-cookies'

Vue.use(Router)
Vue.use(Meta)
Vue.use(VueCookies)

const router = new Router({
  mode: 'hash',
  routes: [
    {
      name: 'Login',
      path: '/',
      component: Login,
    },
    {
      name: 'LandingPage',
      path: '/market',
      component: LandingPage,
    },
  ],
})

//router.replace({ path: '*', redirect: '/' })
export default router
