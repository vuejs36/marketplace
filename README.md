# Marketplace

Marketplace - платформа, созданная специально для Agora Hackathon

## Установка



```bash
npm install
```
## Установка Django-Extensions для поддержки SSL сертификатов
Скачиваем репо из https://github.com/danlamanna/django-extensions/tree/4.1-compat
, переходим в папку, в ней запускаем команду:
```bash
py setup.py install
```

## Установка SSL сертификата
1. Скачать утилиту отсюда: https://github.com/FiloSottile/mkcert
2. Запустить утилиту с командой
```bash
mkcert -install
```
3. Запустить утилиту с командой
```bash
mkcert localhost
```

## Использование

```bash
py manage.py runserver_plus --cert="cert.pem" --key="key.pem"
```

## Обновление Vue.js кода
```bash
npm run build
```
