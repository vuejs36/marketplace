const ImageMinimizerPlugin = require("image-minimizer-webpack-plugin");
module.exports = {
  assetsDir: '',
  publicPath: 'static',
  css: {
    loaderOptions: {
      css: {
        url: false,
      },
    },
  },
};