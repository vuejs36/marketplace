from django.db import models

# Create your models here.

class Product(models.Model):
    product_name = models.CharField(max_length=20)
    product_org = models.CharField(max_length=20)
    product_desc = models.CharField(max_length=300)
    #product_props = models.JSONField(max_length=500)