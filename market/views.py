from django.shortcuts import render, redirect
from market.models import Product
from django.views.generic import TemplateView
from django.views.decorators.cache import never_cache
from rest_framework import status
from rest_framework.decorators import api_view
from django.core import serializers
from rest_framework.response import Response
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.decorators import login_required
from django.contrib.auth.models import User
from django.http import HttpResponse
from django.views.decorators.csrf import csrf_exempt, ensure_csrf_cookie

# Create your views here.

#index_view = never_cache(TemplateView.as_view(template_name='login.html'))
index_view = ensure_csrf_cookie(never_cache(TemplateView.as_view(template_name='index.html')))

#def registrationview(request):
#    username = request.POST['username']
#    mail = request.POST['email']
#    password = request.POST['password']
#    user = User.objects.create_user(username, mail, password)
 #   if user is not None:
 #       login(request, user)
 #       redirect('market/')

@api_view(['GET', 'POST'])
@login_required(login_url='/')
def dbmanage(request):

    if request.method == 'GET':
        return Response(serializers.serialize("json", Product.objects.all()))

    elif request.method == 'POST':
        p = Product(product_name = request.data["prodname"], product_org = request.data["prodorg"], product_desc = request.data["proddesc"])
        p.save()
        return Response()

@api_view(['POST'])
@csrf_exempt
#@ensure_csrf_cookie
def authview(request):
    username = request.POST.get('login', "default")
    password = request.POST.get('password', "default")
    mail = request.POST.get('email', 'test@example.com')
    user = authenticate(request, username=username, password=password)
    if user is not None:
        login(request, user)
        redirect('market/')
    else:
        usersign = User.objects.create_user(username, mail, password)
        if usersign is not None:
            login(request, usersign)
            redirect('market/')
    return Response()

@api_view(['POST'])
@login_required(login_url='/')
def logoutview(request):

    if request.method == 'POST':
        logout(request)
        redirect('')
        return Response()

@api_view(['GET'])
@login_required(login_url='/')
def authcheck(request):
    if request.method == 'GET':
        return Response()
